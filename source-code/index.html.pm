#lang pollen
◊(define-meta title "Home")

◊navigation-sidebar-layout{
  ◊navigation-sidebar{
    ◊navigation-sidebar-link["/projects"]{Projects}}
  ◊navigation-sidebar-layout-content{
    ◊p{Hi, I'm Jonas Schürmann and this is where I will host my programming
       projects and publish my writings in the future. But right now there is
       not much to see yet, I'm still in the progress of building the site
       and migrating all my stuff on here. The only project on here right now
       is ◊a[#:href "/projects/dvcs-for-structured-data/"]{my attempt at
       building a better version control system}.}

    ◊p{You can follow me on Twitter
       ◊a[#:href "https://twitter.com/MazeChaZer"]{@MazeChaZer} if you want to
       stay up to date as I publish more on this site (sorry fediverse folks, I
       haven't had the time to setup my own server yet).}}}
