#lang pollen
◊(define-meta title "Projects")

◊h1[(select 'title metas)]

◊ul{◊li{◊a[#:href "/projects/dvcs-for-structured-data/"]{DVCS for Structured Data}
        ◊p{Explorations into how one could implement decentralized version
           control for structured data based on a category theoretical model
           of patch theory.}}}

◊p{Many of my projects are currently still hosted on
   ◊a[#:href "https://gitlab.com/MazeChaZer"]{GitLab} or
   ◊a[#:href "https://github.com/MazeChaZer"]{GitHub} and will be migrated
   over here soon.}
