let pkgs = import ./nixpkgs.nix;
    websiteLibrary = import ./website-library.nix;
    dvcsForStructuredData = import ./dvcs-for-structured-data.nix;
    assembledSourceCode =
        pkgs.stdenv.mkDerivation {
        name = "website-assembled-source-code";
        src = [ ./source-code ];
        projects = [
            dvcsForStructuredData
        ];
        buildPhase = ''
            projects=($projects)
            cp -r \
                ''${projects[0]}/documentation \
                projects/dvcs-for-structured-data
            chmod -R u+w -- projects/
        '';
        installPhase = ''
            cp -r . $out
        '';
    };
in pkgs.stdenv.mkDerivation {
    name = "website";
    src = assembledSourceCode;
    buildInputs = with websiteLibrary; [ supplyPollenFiles pollen ];
    # One might think that `raco pollen render --recursive .` might be
    # better here, but recursive rendering mutates the cwd and thous
    # breaks a lot of things, so we are required to use pagetrees.
    # This means that only pages referenced by a pagetree are rendered.
    buildPhase = ''
        supply-pollen-files .
        POLLEN=https://jonas-schuermann.name raco pollen render *.ptree
    '';
    installPhase = ''
        raco pollen publish . "$out"
    '';
}
