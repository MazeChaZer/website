import (builtins.fetchTarball {
    name = "nixos-20.03-2b417708c282d84316366f4125b00b29c49df10f";
    url = "https://github.com/nixos/nixpkgs/archive/2b417708c282d84316366f4125b00b29c49df10f.tar.gz";
    sha256 = "0426qaxw09h0kkn5zwh126hfb2j12j5xan6ijvv2c905pqi401zq";
}) {}
